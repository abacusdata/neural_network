import numpy as np
from matplotlib import pyplot as plt

def MinMaxScaler(X):
    # Normalise data
    return (X - X.min(axis = 0)) /(X.max(axis = 0) - X.min(axis = 0))

def unroll_params(theta1, theta2):
    # Unroll all thetas into 1d array
    return (np.concatenate([theta1.flatten(), theta2.flatten()]))

def reform(params, conf):
    # Reshape thetas into matrix
    n_input_layer = conf['input_layer']
    n_hidden_layer = conf['hidden_layer']
    n_output_layer = conf['output_layer']

    theta1 = np.reshape(params[: n_input_layer * n_hidden_layer], (n_input_layer, n_hidden_layer))
    theta2 = np.reshape(params[n_input_layer * n_hidden_layer: ], (n_hidden_layer, n_output_layer))
    return theta1, theta2

def normal(x):
    # Generate normal of vector
    return np.sqrt(sum((x)**2))

def plot_costOverTime(cost_capture):
    print ("Plotting cost Over Time ...")
    plt.plot(cost_capture)
    plt.title("Cost over iterations")
    plt.ylabel("Cost")
    plt.xlabel("Iterations")
    plt.savefig("output/costOverTime.png")
    print('Complete. Check output folder')
    print('Close plot to continue ...')
    plt.show()

def plot_learning_curve(error_train, error_val):
    plt.plot(error_train, label = 'train')
    plt.plot(error_val, label = 'validation')
    plt.xlabel('m')
    plt.ylabel('error')
    plt.title("Learning curve with train and validation")
    plt.legend()
    print('Complete. Check output folder')
    print('Close plot to continue ...')
    plt.savefig("output/train_validation_learning_curve.png")
    plt.show()

def plot_learning_curve_with_lambda(error_train_with_lambda, error_val_with_lambda, conf):
    lambdas_array = conf['lambdas_array']
    plt.plot(lambdas_array, error_train_with_lambda, label = 'train')
    plt.plot(lambdas_array, error_val_with_lambda, label = 'validation')
    plt.xlabel('lambda')
    plt.ylabel('error')
    plt.legend()
    plt.title("Learning curve with altering lambda")
    print('Complete. Check output folder')
    print('Close plot to continue ...')
    plt.savefig("output/learning_curve_w_lambda.png")
    plt.show()

def compute_kFold_crossValidation(X, y, K = 5):
    #TODO: Debug this code: we typically see very high accuracy in first loop i.e. 92, 22, 25, 24
    Xvalidate_accuracy =  []
    chunksize = int(m//K)
    for i in range(K):
        X_train = np.concatenate([X[:chunksize * i, :], X[chunksize * (i + 1):, :]])
        y_train = np.concatenate([y[:chunksize * i, :], y[chunksize * (i + 1):, :]])

        X_test = X[chunksize * i: chunksize * (i + 1), :]
        y_test = y[chunksize * i: chunksize * (i + 1)]
        params, _prediction, _cost_capture = neural_network(X_train, y_train, conf)
        cross_validation_accuracy = calculate_test_accuracy(X_test, y_test, params)
        Xvalidate_accuracy.append(cross_validation_accuracy)
    return Xvalidate_accuracy

def split_data(X, y):
    # Split data into Train (70%), CV(20%) and Test(10%)

    X_train, y_train = X[:100, :], y[: 100, :]
    X_val, y_val = X[100: 150, :], y[100: 150, :]
    X_test, y_test = X[150: 178, :], y[150: 178, :]

    return X_train, y_train, X_val, y_val, X_test, y_test

def one_hot_encoder(y):
    matrix = np.zeros(shape=[y.shape[0], y.max() + 1])
    matrix[np.arange(len(y)), y] = 1
    return matrix