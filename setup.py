from setuptools import setup, find_packages

setup(
    name='network_wine_nisarg',
    author='Winson Lam',
    author_email='winsonv@hotmail.com',
    version='0.1',
    description='Neural Network Interview Challenge by Nisarg of VectorAI',
    install_requires=[open('requirements.txt').read().splitlines()]
)