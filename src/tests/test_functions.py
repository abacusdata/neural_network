import numpy as np
from src.helper import normal, one_hot_encoder
from src.utils import sigmoid, reLU, random_initialise_theta

conf = {
    "input_layer": 3,
    "hidden_layer": 4,
    "output_layer": 2,
    "iterations": 10,
    "epsilon": 1e-4,
    "lambda": 0.0001,
    "lambdas_array": [1e-7, 2e-7, 5e-7, 1e-6, 2e-6, 5e-6, 1e-5, 2e-5, 5e-5, 1e-4]
    }

y = np.array([0, 1, 0, 1, 0, 1])

def test_sigmoid():
    assert sigmoid(0) == 0.5

def test_reLu():
    assert list(reLU(np.array([-3, -2, -1, 0, 1, 2, 3]), grad=False)) == [0, 0, 0, 0, 1, 2, 3]
    assert list(reLU(np.array([-3, -2, -1, 0, 1, 2, 3]), grad=True)) == [0, 0, 0, 0, 1, 1, 1]

def test_normal():
    assert normal(np.array([1, 2, 3, 4])).round(2) == 5.48

def test_random_initialise_theta():
    theta1, theta2 = random_initialise_theta(conf)
    assert theta1.shape[0] == conf['input_layer']
    assert theta1.shape[1] == conf['hidden_layer']
    assert theta2.shape[0] == conf['hidden_layer']
    assert theta2.shape[1] == conf['output_layer']

def test_one_hot_encoder():
    y_hot = one_hot_encoder(y)
    assert y_hot[0, 0] == 1
    assert y_hot[1, 0] == 0
    assert y_hot[2, 0] == 1
    assert y_hot[3, 0] == 0
    assert y_hot.shape == (6, 2)

