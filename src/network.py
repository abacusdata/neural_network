import numpy as np
from matplotlib import pyplot as plt
import json
import logging
logger = logging.getLogger("nnetwork")
from helper import MinMaxScaler, unroll_params, normal, reform, split_data, one_hot_encoder
from helper import plot_costOverTime, plot_learning_curve, plot_learning_curve_with_lambda
from utils import compute_numerical_gradient, sigmoid, reLU, random_initialise_theta


def hypothesis(X, params, conf):
    # Make predictions
    # Params is an array
    theta1, theta2 = reform(params, conf)
    return sigmoid(np.dot(sigmoid(np.dot(X, theta1)), theta2))

def cost_function(X, y, params):
    # Compute cost
    m = len(X)
    hypothesis_theta = hypothesis(X, params, conf)
    cost = (1/m) * np.sum((-y * np.log(hypothesis_theta)) - (1 - y)*(np.log(1 - hypothesis_theta)))
    return cost

def train_validation_learning_error(X_train, y_train, X_val, y_val):

    error_train = np.zeros([len(X_train), 1])
    error_val = np.zeros([len(X_train), 1])

    for i in range(len(X_train)):
        params, _l2, _cost_capture = neural_network(X_train[:i+1, :], y_train[:i+1, :], conf)
        J = cost_function(X_train[:i+1, :], y_train[:i+1, :], params)
        error_train[i] = J
        error_val[i] = (1/len(X_val)) * np.sum((hypothesis(X_val, params, conf) - y_val)**2)

    return error_train, error_val

def train_validation_learning_error_with_lambda(X_train, y_train, X_val, y_val, conf):

    lambdas_array = conf['lambdas_array']
    logging.warning('Possible lambda values could destroy the code')

    error_train_with_lambda = []
    error_val_with_lambda = []

    print ("Plotting error vs lambda. This may take a few minutes ...")

    for _lambda in lambdas_array:
        params, _l2, _cost_capture = neural_network(X_train, y_train, conf)
        J = cost_function(X_train, y_train, params)
        error_train_with_lambda.append(J)
        error_val_with_lambda.append((1/len(X_val)) * np.sum((hypothesis(X_val, params, conf) - y_val)**2))

    return error_train_with_lambda, error_val_with_lambda


def calculate_training_accuracy(prediction, y):
    # Calculates accuracy by comparing hypothesis with test
    counter = 0
    for i in range(len(prediction)):
        if np.array_equal(prediction[i].round(), y[i]):
            counter += 1
    accuracy = counter/len(prediction)*100
    return accuracy

def calculate_test_accuracy(X_test, y_test, params):
    prediction = hypothesis(X_test, params, conf)
    counter = 0
    for i in range(len(prediction)):
        if np.array_equal(prediction[i].round(), y[i]):
            counter += 1
    accuracy = counter/len(prediction)*100
    return accuracy


def forward_propagation(X, theta1, theta2):
    l0 = X
    z1 = np.dot(l0, theta1)
    l1 = sigmoid(z1)
    z2 = np.dot(l1, theta2)
    l2 = sigmoid(z2)
    return l0, l1, l2

def back_propagation(y, l2, l1, theta2):
    error = y - l2
    delta_l2 = error * sigmoid(l2, grad=True)
    delta_l1 = delta_l2.dot(theta2.T) * sigmoid(l1, grad=True)
    return delta_l1, delta_l2

def compute_neural_network_gradient(delta_l1, delta_l2, l0, l1):
    return np.concatenate([np.dot(delta_l1.T, l0).T.flatten(), np.dot(delta_l2.T, l1).T.flatten()])

def neural_network(X, y, conf):

    # Randomly intialise theta to break symmetry
    theta1, theta2 = random_initialise_theta(conf)
    lambda_ = conf['lambda']
    regularisation = conf['regularisation']

    m = X.shape[0]
    cost_array = []

    for i in range(conf['iterations']):
        # Forward Propagation
        l0, l1, l2 = forward_propagation(X, theta1, theta2)

        # Back Propagation
        delta_l1, delta_l2 = back_propagation(y, l2, l1, theta2)

        if regularisation:
            theta2 += np.dot(delta_l2.T, l1).T * (1/m) + lambda_ * theta2
            theta1 += np.dot(delta_l1.T, l0).T * (1/m) + lambda_ * theta1
        else:
            theta2 += np.dot(delta_l2.T, l1).T
            theta1 += np.dot(delta_l1.T, l0).T


        params = unroll_params(theta1, theta2)
        cost_array.append(cost_function(X, y, params))

    return params, l2, cost_array





# Read data
data = np.genfromtxt('data/wine.csv', delimiter=',', skip_header=1)

# Shuffle
np.random.shuffle(data)

# Feature variables
X = data[:, 1:]
X = MinMaxScaler(X)

# Target
# make y into one hot encoder
y = (data[:, 0] - 1).astype(int)
y = one_hot_encoder(y)

with open("conf.json") as f:
    conf = json.load(f)

def main():
    
    print ("Details of Neural Network:")
    logger.info("Details of Neural Network:")
    for i in conf:
        print ("{} : {}".format(i, conf[i]))
    
    print ("\n")
    print ("Training Model ...")

    X_train, y_train, X_val, y_val, X_test, y_test = split_data(X, y)

    params, prediction, cost_capture = neural_network(X_train, y_train, conf)

    training_accuracy = calculate_training_accuracy(prediction, y)
    test_accuracy = calculate_test_accuracy(X_test, y_test, params)

    print ("Training accuracy: {} %".format(training_accuracy))
    print("Test accuracy: {} %".format(test_accuracy))
    print ("\n")
    print ("Evaluating Neural Network Performance.")

    plot_costOverTime(cost_capture)

    if conf["plot_learning_curve"]:
        error_train, error_val = train_validation_learning_error(X_train, y_train, X_val, y_val)
        plot_learning_curve(error_train, error_val)

    if conf["plot_learning_curve_with_lambda"]:
        error_train_with_lambda, error_val_with_lambda = train_validation_learning_error_with_lambda(X_train, y_train, X_val, y_val, conf)
        plot_learning_curve_with_lambda(error_train_with_lambda, error_val_with_lambda, conf)

main()