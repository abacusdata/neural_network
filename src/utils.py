import numpy as np

def sigmoid(x, grad = False):
    # Activation function
    if grad == False:
        return (1/ (1 + np.exp(-x)))
    if grad == True:
        return (x * (1 - x))


def reLU(x, grad = False):
    # Activation function
    if grad == False:
        x[x <= 0] = 0
        return x
    if grad == True:
        x[x <= 0] = 0
        x[x > 0] = 1
        return x

def random_initialise_theta(conf):

    n_input_layer = conf['input_layer']
    n_hidden_layer = conf['hidden_layer']
    n_output_layer = conf['output_layer']
    e = conf['epsilon']

    theta1 = np.random.random([n_input_layer, n_hidden_layer]) * 2 * e - 1 * e
    theta2 = np.random.random([n_hidden_layer, n_output_layer]) * 2 * e - 1 * e

    return theta1, theta2

def compute_numerical_gradient(X, y, params):
    # Generate numerical gradient checking using J(x+e) - J(x-e)/2e
    chkgrad = np.zeros(params.shape)
    perturb = np.zeros(params.shape)
    e = 1e-4

    for p in range(len(params)):
        perturb[p] = e
        loss2 = cost_function(X, y, params + perturb)
        loss1 = cost_function(X, y, params - perturb)
        chkgrad[p] = (loss2 - loss1) / (2 * e)
        perturb[p] = 0
    return chkgrad