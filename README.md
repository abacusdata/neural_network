# Neural Network on Wine Data

## Overview

- The aim of this project is to classy wine types 1, 2 or 3 using the 13 attributes via neural network. Plots are outputted after each run, please close the plot window to continue running the script.

- User can choose to change the parameters in the conf.json file to change the structure of the neural network, set lambda and iterations ... etc

- This neural network repo is intended to be run on a Docker host. Please use Docker to run the Docker Image.

- If you intend to run this code locally, follow the instructions below:

## Getting Started

#### Create a virtual environment

```bash
conda create --name network python=3.6
```

#### Install dependencies

**Requirements**
```bash
pip install -r requirements.txt
```

## Authors

* **Winson Lam**

## Notes

- Despite the accuracy is 100%. Please call Winson Lam on 07988 858 661 to discuss the evaluation results.
